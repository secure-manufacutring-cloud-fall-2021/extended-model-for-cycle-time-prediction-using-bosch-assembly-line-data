#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score,mean_squared_error,mean_absolute_error
from sklearn import tree
from sklearn.datasets import load_iris


from sklearn.tree import export_graphviz

import seaborn as sns
from matplotlib import pyplot as plt


# In[2]:


df = pd.read_csv("regressionDisruptive.csv")


# In[3]:


df.head()


# In[4]:


del df["Id"]
#del df["NumberOfStations"]
df.head()


# In[5]:


target = df.CycleTime
x_train, x_test, y_train, y_test = train_test_split(df, target, test_size = .2)
del x_train["CycleTime"]
del x_test["CycleTime"]


# In[11]:


RF_SEED = 400
regressor = RandomForestRegressor(n_estimators=1000, random_state=RF_SEED)
regressor.fit(x_train, y_train)
 


# In[12]:


predictions = regressor.predict(x_test)


# In[13]:


mse = mean_squared_error(y_test, predictions)
mae = mean_absolute_error(y_test, predictions)
print(mse)
print(mae)


# In[90]:


# Extract single tree
estimator = regressor.estimators_[5]


# In[97]:


from sklearn.tree import export_graphviz
# Export as dot file
export_graphviz(estimator, 
                out_file='tree.dot', 
                rounded = True, proportion = False, 
                precision = 2, filled = True)


# In[7]:


import pandas as pd
from sklearn.datasets import load_boston
from sklearn.ensemble import RandomForestRegressor
from sklearn import tree
#from dtreeviz.trees import dtreeviz # will be used for tree visualization
from matplotlib import pyplot as plt
plt.rcParams.update({'figure.figsize': (12.0, 8.0)})
plt.rcParams.update({'font.size': 14})


# In[ ]:


plt.figure(figsize=(20,20))
_ = tree.plot_tree(regressor.estimators_[0], feature_names=df.columns, filled=True)


# In[98]:


# Convert to png
from subprocess import call
call(['dot', '-Tpng', 'tree.dot', '-o', 'tree.png', '-Gdpi=600'])


# In[60]:


DecisionTreeRegModel = DecisionTreeRegressor(max_depth = 34)
print(x_train)
print(y_train)
DecisionTreeRegModel.fit(x_train, y_train)


# In[61]:


y_pred = DecisionTreeRegModel.predict(x_test)


# In[62]:


mse = mean_squared_error(y_test, y_pred)
mae = mean_absolute_error(y_test, y_pred)
print(mse)
print(mae)


# In[37]:


print(y_pred)
print(y_test)


# In[10]:


text_representation = tree.export_text(DecisionTreeRegModel)
print(text_representation)


# In[11]:


export_graphviz(DecisionTreeRegModel,out_file='tree_limited.dot', feature_names = x_train.columns,
                rounded = True, proportion = False, precision = 2, filled = True, max_depth = 4)


# In[ ]:





# In[63]:


from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
 
from matplotlib.lines import Line2D
from scipy.stats import pearsonr


# In[ ]:




